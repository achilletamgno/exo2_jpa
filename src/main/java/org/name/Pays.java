package org.name;

import java.util.Set;
import java.util.TreeSet;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;



@Entity
@Table (name="t_pays")
public class Pays {

	@Id @GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	
	@Column(name="last_name",length=40)
	private String nom;
		
	@OneToMany(cascade=CascadeType.PERSIST,fetch=FetchType.EAGER,mappedBy="pays")
	private Set<Port>  listePort = new TreeSet<Port>();
	
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	

	public Set<Port> getListePort() {
		return listePort;
	}

	public void setListePort(Set<Port> listePort) {
		this.listePort = listePort;
	}

	@Override
	public String toString() {
		return "Pays [id=" + id + ", nom=" + nom + ", listePort=" + listePort + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((listePort == null) ? 0 : listePort.hashCode());
		result = prime * result + ((nom == null) ? 0 : nom.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Pays other = (Pays) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (listePort == null) {
			if (other.listePort != null)
				return false;
		} else if (!listePort.equals(other.listePort))
			return false;
		if (nom == null) {
			if (other.nom != null)
				return false;
		} else if (!nom.equals(other.nom))
			return false;
		return true;
	}

    
	
}
